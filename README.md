# Quickstart Docker

### L'environnement

1. Monter le docker
```
docker-compose build
```

2. Up Docker
```
docker-compose up
```

1. Depuis votre repertoire de travail -> Lancer l'environnement python36 d'anaconda
```
docker run -it -v $(pwd):/home/notebooks -p 8888:8888 sources_notebook /bin/bash -c "jupyter-notebook --notebook-dir=/home/notebooks --no-browser --port=8888 --ip=0.0.0.0 --allow-root "
```

1.bis  Run shell command for notebook on start

``` 
jupyter-notebook --notebook-dir=/home/notebooks --no-browser --port=8888 --ip=0.0.0.0 --allow-root &
```

### Si vous voulez ajouter d'autre environnement Python Anaconda dans Jupyter

```
python -m ipykernel install --user --name envPython36 --display-name "Python (envPython36)"
```

# Containers Python

### lancer le python36 (hors anaconda)
```
docker exec -it DEFAULT_PYTHON sh -c "cd /srv/python; sh"
```

# Autre commandes dockers

### Lister les conteneurs
```
docker ps
```
### Arrêter tous les conteneurs
```
docker stop $(docker ps -q)
```

### Supprimer tous les conteneurs
```
docker rm $(docker ps -a -q)
```

### Cette commande peut être combinée avec celle d'arrêt pour arrêter et supprimer tous les conteneurs :
```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```
### Supprimer tout les images
```
sudo docker rmi $(docker images -q)
```
